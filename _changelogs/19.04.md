---
layout: changelog
title: 19.04
sorted: 1904
date: 2019-04-18
css-include: /css/main.css
---

## New Features:

* Possibility to hide and show results of command entry via context menu
* [Maxima, Octave, Python, R] Add a way to specify the path to the local documentation in the settings. By default, this path didn't specified and Cantor uses online documentation.
* Huge improvment of variables managment: better parsing, option to turn on and turn off variable managment, better GUI performance by limitation of value size (too big for showing values replaced by "<too big variable>" text

## Important bug fixes:

* [Sage] Fix execuation for unsystem Sage installation
* [Julia] Fix bug, that suppressing output don't work

