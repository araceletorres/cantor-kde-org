---
layout: post
title: Cantor in KDE Applications 16.08
date: 2016-08-18
---

The new version of Cantor was released with [KDE Applications 16.08](https://www.kde.org/announcements/announce-applications-16.08.0.php) bundle. Most of the changes were implemented during [LaKademy 2016](http://blog.filipesaraiva.info/?p=1839).The main changes are:

* libcantorlibs is using the same version number used by KDE Applications bundle;
* Fix tab-completion when the completion is selected using the mouse;
* Support to Sage > 7.0 version;
* LaTeX rendering is back to Sage backend;
* Fix the crash when Sage backend is closed;
* Add new commands to the list of plot commands in Octave backend.

More bugs were solved too. Read the complete changelog for this version of Cantor in [KDE Applications 16.08 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0).
