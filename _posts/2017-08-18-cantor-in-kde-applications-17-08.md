---
layout: post
title: Cantor in KDE Applications 17.08
date: 2017-08-18
---

Cantor new version was released with [KDE Applications 17.08](https://www.kde.org/announcements/announce-applications-17.08.0.php) bundle. This version is more focused in stability and bug fixes.

For this release we implemented a new "programming language version recommendation" to inform users about the best programming language version supported by the backend.

Scilab 6.0 was tested and is correctly supported by Scilab backend.

Other small new feature is the LaTeX rendering with dark themes.

Read the complete changelog for this version of Cantor in [KDE Applications 17.08 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0).
